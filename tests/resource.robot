*** Settings ***
Documentation    A resource file with reusable keywords and variables
...
...              The system specific keywords created here from our own
...              domain specific language
Library         SeleniumLibrary
Library         OperatingSystem
Library         SeleniumLibrary



*** Keywords ***
Goto Academy URL
    Create Webdriver    Chrome
    Maximize Browser Window
    Go To               ${url}

Fill the login form
    [Arguments]    ${username}  ${password}
    Input Text    username  ${username}
    Input Password    password  ${password}
    Click Button    signInBtn


Close the browser
    Close Browser

Wait until element is located in the page
    [Arguments]    ${element}
    Wait Until Element Is Visible      ${element}

Verify card titles in the Shop Page


*** Variables ***
${valid_password}  learning
${invalid_password}    12345
${username}         rahulshettyacademy
${url}              https://rahulshettyacademy.com/loginpagePractise/