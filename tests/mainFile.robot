*** Settings ***
Library     SeleniumLibrary
Documentation    Learning Robot Framework
Library    SeleniumLibrary
Test Setup    Goto Academy URL
#Test Teardown    Close Browser
Resource    resource.robot


*** Variables ***
${Error_Message_Locator}    css:.alert-danger
${Shop_page_load}           css:.nav-link


*** Test Cases ***
Validate Unsuccessfull Login
    Fill The Login Form    ${username}  ${invalid_password}
    Wait Until Element Is Located In The Page    ${Error_Message_Locator}
    Verify Error Message

Validate Cards display in the login page
   Fill the login Form  ${username}  ${valid_password}
   Wait Until Element Is Located In The Page    ${Shop_page_load}

Verify card titles in the Shop Page
    Fill the login Form  ${username}  ${valid_password}
    Wait until element is located in the page    ${Shop_page_load}
    @{expectedList} =   Create List    iPhone X     Samsung Note 8      Nokia Edge      Blackberry
    @{elements} =   Get WebElements    css:.card-title
    FOR  ${element}  IN  @{elements}
        Log     ${element.text}
    END


*** Keywords ***
Verify Error Message
    ${result}=  Get Text    ${Error_Message_Locator}
    Should Be Equal As Strings    ${result}     Incorrect username/password.
    Element Text Should Be  ${Error_Message_Locator}    Incorrect username/password.